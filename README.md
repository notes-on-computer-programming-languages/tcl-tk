# tcl-tk

Event-driven, functional, imperative, object-oriented, dynamic programming language. https://en.m.wikipedia.org/wiki/Tcl

## Official documentation
### Memory management
* [*memory management*](https://wiki.tcl-lang.org/page/memory+management)
* [*Garbage collection*](https://wiki.tcl-lang.org/page/Garbage+collection)
* [*Reference counting vs. tracing garbage collection*
  ](https://wiki.tcl-lang.org/page/Reference+counting+vs.+tracing+garbage+collection)

## Code contribution repositories (packages)
### [async](https://wiki.tcl-lang.org/page/async)
### [promise](https://wiki.tcl-lang.org/page/promise)
* [*Using async/await to simplify promises*
  ](https://www.magicsplat.com/blog/promise-async/)
  2017-11

## Unofficial documentation
### Tutorials
* [Learn X in Y minutes, where X=Tcl](https://learnxinyminutes.com/docs/tcl/)

## Try it online
* https://tio.run/#tcl

## OOP
* Object Oriented Programming is part of Tcl itself since Tcl 8.6 (2012).
  * [Tcl/Tk 8.6](https://www.tcl.tk/software/tcltk/8.6.html)
  * [Tcl/Tk Documentation](https://www.tcl.tk/doc/)